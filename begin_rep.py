import sys 
import numpy as np
from numpy.lib.function_base import append
# sys argv and eval functions 

# loops initialzation , condition, increment/decremen. 


# num = 19

# for i in range(2,num):
#      if num % i == 0:
#          print("Not Prime") 
#          break
# else:
#     print("Prime")  

# use optimized methods to check prime

# to print index add another variable  

# matrix multiplication without numpy 

# pass by refernce and pass by value understading 

# function - args, variable length, default, keyword 

# global and local in  the same functions 

# factorial and fibonacci numbers (with recursion)

#interfaces python



#itertools default dict

 

# def update(x):
#     print('Value of x ',x)
#     print('Memory Address of x before update',id(x))
#     x=8
#     print('Value of x after update',x)
#     print('Memory Address of x after update',id(x))

# a=10
# print('Value of a ',a)
# print('Memory Address of a before update',id(a))
# update(a)
# print('Value of a ',a)
# print('Memory Address of a before update',id(a))
# ----------------------------------------------------------
# Mutable object as argument - List
# ----------------------------------------------------------
# def update(x):
#     print('inside function, before update',spam)
#     print('Memory address of list inside function, before update',id(spam))
#     spam[1]=-3
#     print('inside function, after update',spam)
#     print('Memory address of list inside function, after update',id(spam))


# spam=[1,2,3]
# print('Items in list',spam)
# print('Memory address of list',id(spam))
# update(spam)
# print('Items in updated list',spam)
# print('Memory Address of updated list',id(spam))

# def max_prod_naive(arr):
#     product = 0
#     for i in range(len(arr)):
#         for j in range(i+1,len(arr)):
#             product = max(product,arr[i]*arr[j])
#     return product
# #Fast approach
# def max_prod_fast(arr):
#     p1 = max(arr)
#     arr.remove(p1)
#     p2 = max(arr)
#     return p1*p2
# # Stress test

# from random import randint


# import sys
  
# class MaxHeap:
  
#     def __init__(self, maxsize):
          
#         self.maxsize = maxsize
#         self.size = 0
#         self.Heap = [0] * (self.maxsize + 1)
#         self.Heap[0] = sys.maxsize
#         self.FRONT = 1
  
#     # Function to return the position of
#     # parent for the node currently
#     # at pos
#     def parent(self, pos):
          
#         return pos // 2
  
#     # Function to return the position of
#     # the left child for the node currently
#     # at pos
#     def leftChild(self, pos):
          
#         return 2 * pos
  
#     # Function to return the position of
#     # the right child for the node currently
#     # at pos
#     def rightChild(self, pos):
          
#         return (2 * pos) +1



def palindrome_check(list_variable):
    if list_variable[::-1] == list_variable:
        print("String is palindrome")
    else:
        print("String is not a palindrome")         




# LC palindrome for integer
# class Solution:
#     def isPalindrome(self, x):
#         if x > 0:
#             temp = x
#             rev_int_elements = []
#             while temp > 0: -121
#                 digit = temp % 10 -1
#                 rev_int_elements.append(digit) -1
#                 temp = temp // 10 - 12
#             org_int_elements = rev_int_elements[::-1] 1
#             return rev_int_elements == org_int_elements 
#         elif x == 0:
#             return True
#         else:
#             return False


def insertion_sort(arr):
        
    for i in range(len(arr)):
        cursor = arr[i]
        pos = i
        
        while pos > 0 and arr[pos - 1] > cursor:
            # Swap the number down the list
            arr[pos] = arr[pos - 1]
            pos = pos - 1
        # Break and do the final swap
        arr[pos] = cursor

    return arr


array_value = [1,3,4,56,7,8]   

print(insertion_sort(array_value))