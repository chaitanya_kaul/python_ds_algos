Jupyter notebook for DS & Algos

1. Algorithm analysis
2. Recursion  and example (factorial)
3. Array based sequences 
4. Stacks, Queue, Deques
5. Linked List & Circular Linked List 
6. Binary Trees
7. Maps
8. Hash Tables
9. Binary Heap
10. Sorted Maps
11. Skip lists
12. Linear Search 
13. Fibonacci Search 
14. Jump Search 
15. Interpolation Search 
16. Exponential Search 
17. Enum Class
